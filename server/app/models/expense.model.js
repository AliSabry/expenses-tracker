const mongoose = require("mongoose");
const { Schema } = mongoose;

const ExpenseSchema = new Schema({
  amount: {
    type: Number,
    required: true,
    trim: true,
  },
  description: {
    type: String,
  },
  created: {
    type: Date,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const ExpenseModel = mongoose.model("Expemse", ExpenseSchema);
module.exports = ExpenseModel;
