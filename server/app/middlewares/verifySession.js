const jwt = require('jsonwebtoken');
const ApiResponse = require('../utils/apiResponse');
const { Status } = require('../utils/constents');
const secret = process.env.JWT_SECRET;

module.exports = (req, res, next) => {
  const token = req.headers['x-access-token'];
  if (!token) {
    return res.status(Status.UNAUTHORIZED).send(ApiResponse([], 'Token is Required'));
  }
  return jwt.verify(token, secret, (err, user) => {
    if (err) {
      let errors = new Error(err.message);
      errors.status = Status.UNAUTHORIZED;
      return next(errors);
    }
    res.status(Status.OK).send({ isValid: true });
  });
};
