const { Status } = require("../utils/constents");
module.exports = (app) => {
  // custom not found err
  app.use((req, res, next) => {
    let error = new Error(`${req.method} => ${req.originalUrl} Not Found`);
    error.status = Status.NOT_FOUND;
    next(error);
  });
  // error handler middleware
  app.use((err, req, res, next) => {
    const status = err.status || Status.SERVER_ERROR;
    res.status(status).send({
      code: status,
      message: err.message || "Internal Server Error",
    });
  });
};
