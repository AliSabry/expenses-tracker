const express = require('express');
const userController = require('../controller/user.controller');
const router = express.Router();

// router.post("/create", expenseController.create);
router.delete('/delete', userController.destroy);
router.put('/update', userController.update);
router.get('/profile', userController.getProfile);
router.put('/password', userController.changePassword);

module.exports = router;
