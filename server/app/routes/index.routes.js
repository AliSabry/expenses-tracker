const { API_CONST } = require('../utils/constents');
const authJwt = require('../middlewares/auth.jwt');
module.exports = (app) => {
  // Auth API Login && Register
  app.use(`${API_CONST}/auth`, require('./auth.routes'));
  //middleware auth for Routes
  app.use((req, res, next) => {
    authJwt(req, res, next);
  });
  // Protexted Routes
  app.use(`${API_CONST}/expense`, require('./expense.routes'));
  app.use(`${API_CONST}/user`, require('./user.routes'));
};
