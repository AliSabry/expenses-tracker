const express = require('express');
const expenseController = require('../controller/expense.controller');
const router = express.Router();
// Auth and sign up
router.post('/create', expenseController.create);
router.get('/get', expenseController.getExpense);
router.delete('/:expense_id', expenseController.destroy);
router.put('/:expense_id', expenseController.update);

module.exports = router;
