const express = require('express');
const authController = require('../controller/auth.controller');
const router = express.Router();
// Auth and sign up
router.post('/register', authController.register);
router.post('/login', authController.login);
router.post('/verify', require('../middlewares/verifySession'));

module.exports = router;
