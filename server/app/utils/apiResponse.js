module.exports = (data, error) => {
  return {
    success: error.length === 0,
    data,
    error,
  };
};
