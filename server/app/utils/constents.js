module.exports = {
  API_CONST: "/api/v1",
  Status: {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    SERVER_ERROR: 500,
  },
  Password: {
    minLength: 8,
    bcryptFactor: 12,
  },
};
