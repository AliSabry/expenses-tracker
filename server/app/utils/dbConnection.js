const mongoose = require('mongoose');
const config = require('../config.json');
const DB_URL = process.env.DB_URL || config.DB_URL;

module.exports = () => {
  mongoose
    .connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .then(() => console.log('Connected to DB successfully'))
    .catch((err) => console.error(`DB connection Failed ${err}`));
};
