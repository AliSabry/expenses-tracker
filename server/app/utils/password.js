const bcrypt = require("bcryptjs");
const { Password } = require("./constents");

module.exports = {
  hashing(password) {
    const salt = bcrypt.genSaltSync(Password.bcryptFactor);
    const hash = bcrypt.hashSync(password, salt);
    return hash;
  },
  matching(password, hashed) {
    const isMatch = bcrypt.compareSync(password, hashed);
    return isMatch;
  },
  checkLength(password) {
    return password.length >= Password.minLength ? true : false;
  },
};
