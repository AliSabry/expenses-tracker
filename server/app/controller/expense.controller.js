const expenseModel = require('../models/expense.model');
const ApiResponse = require('../utils/apiResponse');
const mongoErrors = require('mongo-error-handler');
const { Status } = require('../utils/constents');
class Expense {
  create(req, res, next) {
    const { amount, description, created } = req.body;
    const newExpense = new expenseModel({
      amount,
      description,
      created,
      user: req._id,
    });
    return newExpense
      .save()
      .then((expense) => {
        return res.status(Status.OK).send(ApiResponse({ msg: 'Expense created successfuly', id: expense._id }, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }
  getExpense(req, res, next) {
    const { _id } = req;
    let { month } = req.query;

    //  query for duration start date and end date
    const now = new Date();
    if (month) {
      month = parseInt(month);
      now.setMonth(month);
    }
    // GET THE MONTH AND YEAR OF THE SELECTED DATE.
    let selectedMonth = now.getMonth(),
      year = now.getFullYear();
    const firstDay = new Date(year, selectedMonth, 1);
    const lastDay = new Date(year, selectedMonth + 1, 1);
    const daysInMonth = new Date(year, selectedMonth + 1, 0).getDate();

    const query = {
      user: _id,
      created: {
        $gt: firstDay,
        $lte: lastDay,
      },
    };

    const Statistics = {};

    return expenseModel
      .find(query)
      .sort({ created: 'desc' })
      .populate('user', ['-password', '-__v'])
      .then((expenses) => {
        if (expenses.length > 0) {
          // max amount
          Statistics.max = expenses.map((item) => item.amount).reduce((prev, next) => (prev > next ? prev : next));
          // total expense
          Statistics.total = expenses.map((item) => item.amount).reduce((prev, next) => prev + next);
          // Avg Expenses
          Statistics.avg = (Statistics.total / daysInMonth).toFixed(2);
        }
        return res.status(Status.OK).send(ApiResponse({ expenses, Statistics }, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        console.log(err);
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }
  destroy(req, res, next) {
    const { expense_id } = req.params;
    expenseModel
      .findOneAndDelete({ _id: expense_id })
      .then((expense) => {
        return res.status(Status.OK).send(ApiResponse({ msg: 'Expense deleted successfully' }, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }
  update(req, res, next) {
    const { expense_id } = req.params;
    const updates = req.body;
    expenseModel
      .findByIdAndUpdate({ _id: expense_id }, updates, {
        runValidators: true,
        new: true,
      })
      .then((expense) => {
        return res.status(Status.OK).send(ApiResponse({ msg: 'Expense Updated successfuly', id: expense._id }, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }

}
module.exports = new Expense();
