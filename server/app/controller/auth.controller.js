const mongoErrors = require('mongo-error-handler');
const jwt = require('jsonwebtoken');
const userModel = require('../models/user.model');
const { Status } = require('../utils/constents');
const passwordHandler = require('../utils/password');
const ApiResponse = require('../utils/apiResponse');
class User {
  register(req, res, next) {
    let { name, email, password, joined } = req.body;
    // check user password exist and length
    if (password && passwordHandler.checkLength(password)) {
      // hash password if exist
      password = passwordHandler.hashing(password);
    }
    const newUser = new userModel({
      name,
      email,
      password,
      joined,
    });
    return newUser
      .save()
      .then((user) => {
        return res.status(Status.OK).send(ApiResponse({ msg: 'Account created successfuly' }, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }
  login(req, res, next) {
    const { email, password } = req.body;
    //   check username and password if exist
    if (!email || !password) {
      return res.status(Status.BAD_REQUEST).send(ApiResponse([], 'email or password are required !'));
    }

    // check user email and password
    return userModel
      .findOne({ email })
      .then((user) => {
        if (!user) {
          return res.status(Status.BAD_REQUEST).send(ApiResponse([], 'email or password invaild'));
        }
        // match user password
        if (passwordHandler.matching(password, user.password)) {
          // create user session with jwt and login
          const secret = process.env.JWT_SECRET;
          const expire = process.env.JWT_EXPIRATION;
          const token = jwt.sign({ sub: user._id }, secret, {
            expiresIn: expire,
          });
          return res.status(Status.OK).send(ApiResponse({ token }, []));
        }
        return res.status(Status.UNAUTHORIZED).send(ApiResponse([], 'email or password invaild'));
      })
      .catch((err) => {
        console.log(err);

        let error = new Error(mongoErrors(err));
        error.status = Status.BAD_REQUEST;
        next(error);
      });
  }
}

module.exports = new User();
