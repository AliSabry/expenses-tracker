const userModel = require('../models/user.model');
const ApiResponse = require('../utils/apiResponse');
const mongoErrors = require('mongo-error-handler');
const passwordHandler = require('../utils/password');
const { Status } = require('../utils/constents');

class User {
  getProfile(req, res, next) {
    const { _id } = req;
    // TODO add query for duration start date and end date
    return userModel
      .findOne({ _id }, ['-password', '-__v'])
      .then((data) => {
        return res.status(Status.OK).send(ApiResponse(data, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }
  destroy(req, res, next) {
    const { _id } = req;
    userModel
      .findOneAndDelete({ _id: _id })
      .then((user) => {
        return res.status(Status.OK).send(ApiResponse({ msg: 'Account deleted successfully' }, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }
  update(req, res, next) {
    const { _id } = req;
    const updates = req.body;
    userModel
      .findByIdAndUpdate({ _id: _id }, updates, {
        runValidators: true,
        new: true,
      })
      .then((user) => {
        return res.status(Status.OK).send(ApiResponse({ msg: 'User Updated successfuly', id: user._id }, []));
      })
      .catch((err) => {
        let errors = new Error(mongoErrors(err));
        errors.status = Status.BAD_REQUEST;
        next(errors);
      });
  }
  async changePassword(req, res, next) {
    const { _id } = req;
    const { oldPassword, newPassword } = req.body;

    // Check Body password & Length
    if (!oldPassword || !newPassword || !passwordHandler.checkLength(newPassword))
      return res.status(Status.BAD_REQUEST).send(ApiResponse([], { message: 'Old or new password are invalid' }));

    // Get current Saved password
    const { password } = await userModel.findById({ _id }, ['password']);
    // Verify old Password it true check new and update
    if (passwordHandler.matching(oldPassword, password)) {
      // hash new Password and save
      let hashNew = passwordHandler.hashing(newPassword);
      return userModel
        .findOneAndUpdate(
          { _id },
          { password: hashNew },
          {
            runValidators: true,
            new: true,
          }
        )
        .then((user) => {
          return res.status(Status.OK).send(ApiResponse({ msg: 'Password updated successfully', _id: user._id }, []));
        })
        .catch((err) => {
          let errors = new Error(mongoErrors(err));
          errors.status = Status.BAD_REQUEST;
          return next(errors);
        });
    }
    return res.status(Status.BAD_REQUEST).send(ApiResponse([], { message: 'Password not valid' }));
  }
}
module.exports = new User();
