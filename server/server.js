const express = require("express");
const logger = require("morgan");
const cors = require("cors");
require("dotenv").config();
const config = require("./app/config.json");
const app = express();
const PORT = process.env.PORT || config.PORT;
// ----------- connect DB ----------- //
require("./app/utils/dbConnection")();
// ----------- middlewares ----------- //
app.use(logger("dev"));
app.use(cors());
app.use(express.json());
// ----------- Routes ----------- //
require("./app/routes/index.routes")(app);
// ----------- ERRORS ----------- //
require("./app/middlewares/errorHandler")(app);

app.listen(PORT, () => console.log(`server start at port ${PORT}`));
