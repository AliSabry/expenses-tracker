import React from 'react';
import { Button, ModalFooter, FormGroup, Label, Input, FormFeedback } from 'reactstrap';
import { Formik } from 'formik';
import moment from 'moment';
import * as Yup from 'yup';

const AddFormBody = ({ btnTxt = 'Save', onSubmitAction, expense = {} }) => {
  const { amount = '', created = undefined, description = '' } = expense;
  const now = created ? moment(created).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');
  return (
    <Formik
      initialValues={{ amount: amount, created: now, description: description }}
      onSubmit={onSubmitAction}
      validationSchema={Yup.object().shape({
        amount: Yup.number().min(1).required(),
        created: Yup.date().required(),
        description: Yup.string(),
      })}>
      {({ errors, touched, handleBlur, handleChange, handleSubmit, isSubmitting, isValid, values }) => (
        <div>
          <FormGroup>
            <Label>Amount</Label>
            <Input
              invalid={errors.amount && touched.amount}
              valid={!errors.amount && touched.amount}
              name='amount'
              type='number'
              value={values.amount}
              placeholder='Enter Expense amount'
              onBlur={handleBlur}
              onChange={handleChange}></Input>
            {errors.amount && touched.amount && <FormFeedback>{errors.amount}</FormFeedback>}
          </FormGroup>
          <FormGroup>
            <Label>Description</Label>
            <Input
              name='description'
              type='text'
              value={values.description}
              placeholder='Enter Expense description'
              onBlur={handleBlur}
              onChange={handleChange}></Input>
          </FormGroup>
          <FormGroup>
            <Label>Date</Label>
            <Input
              invalid={errors.created && touched.created}
              valid={!errors.created && touched.created}
              value={values.created}
              name='created'
              type='date'
              onBlur={handleBlur}
              onChange={handleChange}></Input>
            {errors.created && touched.created && <FormFeedback>{errors.created}</FormFeedback>}
          </FormGroup>
          <ModalFooter className='justify-content-center m-0 py-3'>
            <Button color='success' className='w-50' disabled={isSubmitting || !isValid} onClick={handleSubmit}>
              {btnTxt}
            </Button>
          </ModalFooter>
        </div>
      )}
    </Formik>
  );
};
export { AddFormBody };
