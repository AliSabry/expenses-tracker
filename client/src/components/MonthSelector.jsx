import React, { Component } from 'react';
import { Label, Input } from 'reactstrap';
import moment from 'moment';

class MonthSelector extends Component {
  handelOnChange = (e) => {
    const month = e.target.value;
    this.props.onChange(month);
  };
  render() {
    const MONTHES = moment.months();
    const { selected } = this.props;

    return (
      <div
        style={{
          marginBottom: 20,
        }}>
        <Label for='monthSelect'>Month </Label>
        {'  '}
        <Input
          style={{ width: 'auto', display: 'inline-block' }}
          type='select'
          name='select'
          id='monthSelect'
          defaultValue={selected}
          onChange={this.handelOnChange}>
          {MONTHES.map((month, index) => (
            <option value={index} key={index}>
              {month}
            </option>
          ))}
        </Input>
      </div>
    );
  }
}

export { MonthSelector };
