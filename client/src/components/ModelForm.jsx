import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Alert } from 'reactstrap';
import { FloatButton } from './FloatButton';
import { apiSaveExpense } from '../api/expense';
import { AddFormBody } from './AddFormBody';

class ModelForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      errors: null,
    };
  }
  // form submit
  handleSubmit = (values, bag) => {
    apiSaveExpense(values)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          const {
            data: { msg },
          } = data;
          this.setState({ errors: <Alert color='success'>{msg}</Alert> });
        }
      })
      .catch((err) => {
        console.log(err.response.data);
        const {
          response: {
            data: { message },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{message ? message : 'Server error'}</Alert> });
        bag.setSubmitting(false);
      })
      .then(() => {
        this.props.onRefresh(true);
      });
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
    this.resetError();
  };

  resetError = () => {
    this.setState({ errors: null });
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <FloatButton onClick={this.toggle} />
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Add Expense</ModalHeader>
          <ModalBody className='pb-0'>
            {errors}
            <AddFormBody onSubmitAction={this.handleSubmit} />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export { ModelForm };
