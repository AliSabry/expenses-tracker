import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap';

import { removeUserSession, getToken } from '../utils/index';
import { apiProfile } from '../api/auth';
import { Link } from 'react-router-dom';

const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState('');
  const toggle = () => setIsOpen(!isOpen);

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggleDropDown = () => setDropdownOpen(!dropdownOpen);

  const handelGetProfile = () => {
    const token = getToken();
    if (token) {
      apiProfile(token)
        .then((user) => {
          const { data } = user;
          if (data.success) {
            const {
              data: { name, email },
            } = data;
            setName(name);
            setEmail(email);
          }
        })
        .catch((err) => {
          console.log(err);
          const {
            response: {
              data: { error },
            },
          } = err;
          console.log(error);
        });
    }
  };

  const handelLogout = () => {
    removeUserSession();
  };

  // const dropdownHover = '.dropdown-item:hover {\
  // background-color: #0366d6;\
  // color: aliceblue;}';

  return (
    <Navbar color='dark' dark expand='md'>
      <style>{`
      .dropdown-item:hover {
        background-color: #0366d6;
        color: aliceblue;
      `}</style>
      <NavbarBrand href='/'>Expenses</NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav className='ml-auto' navbar>
          <Dropdown direction='down' nav isOpen={dropdownOpen} toggle={toggleDropDown}>
            <DropdownToggle nav caret>
              Hi, {name ? name : handelGetProfile()}
            </DropdownToggle>
            <DropdownMenu
              style={{
                left: '-100px',
              }}>
              <DropdownItem className='font-weight-bold' color='muted' header>
                {email}
              </DropdownItem>
              <DropdownItem divider />
              <Link
                to={{
                  pathname: 'Setting',
                  state: { name: name, email: email },
                }}>
                <DropdownItem>Settings</DropdownItem>
              </Link>
              <DropdownItem divider />
              <DropdownItem href='/login' onClick={handelLogout}>
                Logout
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Nav>
      </Collapse>
    </Navbar>
  );
};

export { NavBar };
