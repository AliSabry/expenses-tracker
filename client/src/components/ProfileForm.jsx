import React from 'react';
import { Button, FormGroup, FormFeedback, Label, Input } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Redirect } from 'react-router-dom';

const ProfileForm = ({ handleFormSubmit, user }) => {
  if (!user) return <Redirect push to='/' />;
  const { name = '', email = '' } = user;

  return (
    <React.Fragment>
      {/* Formik Form Handel with Yup validation */}
      <Formik
        initialValues={{ email: email, name: name }}
        onSubmit={handleFormSubmit}
        validationSchema={Yup.object().shape({
          email: Yup.string().email(),
          name: Yup.string(),
        })}>
        {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched, values }) => (
          <div>
            <FormGroup>
              <Label>Email</Label>
              <Input
                invalid={errors.email && touched.email}
                valid={!errors.email && touched.email}
                name='email'
                type='email'
                value={values.email}
                placeholder='example@g.com'
                onBlur={handleBlur}
                onChange={handleChange}></Input>
              {errors.email && touched.email ? <FormFeedback>{errors.email}</FormFeedback> : null}
            </FormGroup>
            <FormGroup>
              <Label>Name</Label>
              <Input
                invalid={errors.name && touched.name}
                valid={!errors.name && touched.name}
                name='name'
                type='name'
                value={values.name}
                placeholder='enter your name'
                onBlur={handleBlur}
                onChange={handleChange}></Input>
              {errors.name && touched.name ? <FormFeedback>{errors.name}</FormFeedback> : null}
            </FormGroup>
            <div className='text-left py-2'>
              <Button color='success' className='w-40' onClick={handleSubmit}  disabled={!isValid || isSubmitting}>
                Update Profile
              </Button>
            </div>
          </div>
        )}
      </Formik>
    </React.Fragment>
  );
};

export { ProfileForm };
