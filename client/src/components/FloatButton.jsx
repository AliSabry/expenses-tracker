import React from 'react';
import { Button } from 'reactstrap';
function FloatButton({ onClick }) {
  const buttonStyle = {
    borderRadius: 50,
    width: 60,
    height: 60,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: 'none',
    boxShadow: '2px 4x 15px 0px #999',
  };
  return (
    <div style={{ position: 'fixed', bottom: 40, right: 40, zIndex:150 }}>
      <Button onClick={onClick} color='primary' style={buttonStyle}>
        <i style={{ fontSize: '1.5rem' }} color='light' className='fa fa-plus'></i>
      </Button>
    </div>
  );
}

export { FloatButton };
