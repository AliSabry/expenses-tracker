import React, { useState } from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

const ConfirmModel = (props) => {
  const { onDelete, item } = props;
  const [modal, setModal] = useState(false);
  const { setLabel = undefined } = item;
  const toggle = () => {
    setModal(!modal);
  };

  return (
    <div className='btn text-danger p-0'>
      {setLabel ? (
        <Button className='btn' color='danger' onClick={toggle} data-id={item._id}>
          {setLabel}
        </Button>
      ) : (
        <i className='fas fa-trash-alt' onClick={toggle} data-id={item._id} style={{ fontSize: '1.3rem' }}></i>
      )}
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Confirm</ModalHeader>
        <ModalBody>You will delete {item.description}?</ModalBody>
        <ModalFooter>
          <Button color='Danger' data-id={item._id} onClick={onDelete}>
            OK
          </Button>{' '}
          <Button color='secondary' onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export { ConfirmModel };
