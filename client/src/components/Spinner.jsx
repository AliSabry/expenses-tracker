import React from 'react';

const Spinner = ({ size, color }) => {
  let spinnerSize = size ? size : 30;
  let spinnerColor = color ? color : 'gray';
  return (
    <div
      style={{
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
      }}>
      <i className='fas  fa-circle-notch fa-spin' style={{ fontSize: spinnerSize, color: spinnerColor }} />
    </div>
  );
};

export { Spinner };
