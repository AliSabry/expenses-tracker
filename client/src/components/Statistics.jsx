import React from 'react';
import { Button, Badge } from 'reactstrap';

const Statistics = ({ data }) => {
  return (
    <div>
      {data.total && (
        <React.Fragment>
          <Button size='sm' className='m-2' color='dark'>
            Total{' '}
            <Badge className='mx-1 p-1' color='light'>
              {data.total} AED
            </Badge>
          </Button>
          <Button size='sm' color='dark'>
            Daily{' '}
            <Badge className='mx-1 p-1' color='success'>
              {data.avg} AED
            </Badge>
          </Button>
          <Button size='sm' className='m-2' color='dark'>
            Max{' '}
            <Badge className='mx-1 p-1' color='danger'>
              {data.max} AED
            </Badge>
          </Button>
          <hr />
        </React.Fragment>
      )}
    </div>
  );
};

export { Statistics };
