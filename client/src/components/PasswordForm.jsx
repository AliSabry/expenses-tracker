import React from 'react';
import { Button, FormGroup, FormFeedback, Label, Input } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';

const PasswordForm = ({ handleFormSubmit }) => {
  return (
    <React.Fragment>
      {/* Formik Form Handel with Yup validation */}
      <Formik
        initialValues={{ oldPassword: '', newPassword: '', confirmPassword: '' }}
        onSubmit={handleFormSubmit}
        validationSchema={Yup.object().shape({
          oldPassword: Yup.string().required().min(8),
          newPassword: Yup.string().required().min(8),
          confirmPassword: Yup.string()
            .required()
            .oneOf([Yup.ref('newPassword')], 'Both password need to be the same'),
        })}>
        {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched, values }) => (
          <div>
            <FormGroup>
              <Label>Password</Label>
              <Input
                invalid={errors.oldPassword && touched.oldPassword}
                valid={!errors.oldPassword && touched.oldPassword}
                name='oldPassword'
                type='password'
                placeholder='enter your current password'
                onBlur={handleBlur}
                onChange={handleChange}></Input>
              {errors.password && touched.password ? <FormFeedback>{errors.password}</FormFeedback> : null}
            </FormGroup>
            <FormGroup>
              <Label>New password</Label>
              <Input
                invalid={errors.newPassword && touched.newPassword}
                valid={!errors.newPassword && touched.newPassword}
                name='newPassword'
                type='password'
                placeholder='Enter your new password'
                onBlur={handleBlur}
                onChange={handleChange}></Input>
              {errors.newPassword && touched.newPassword ? <FormFeedback>{errors.newPassword}</FormFeedback> : null}
            </FormGroup>
            <FormGroup>
              <Label>Confirm password</Label>
              <Input
                invalid={errors.confirmPassword && touched.confirmPassword}
                valid={!errors.confirmPassword && touched.confirmPassword}
                name='confirmPassword'
                type='password'
                placeholder='Confirm new password'
                onBlur={handleBlur}
                onChange={handleChange}></Input>
              {errors.confirmPassword && touched.confirmPassword ? (
                <FormFeedback>{errors.confirmPassword}</FormFeedback>
              ) : null}
            </FormGroup>
            <div className='text-left py-2'>
              <Button color='dark' className='w-40' onClick={handleSubmit} disabled={!isValid || isSubmitting}>
                Change Password
              </Button>
            </div>
          </div>
        )}
      </Formik>
    </React.Fragment>
  );
};

export { PasswordForm };
