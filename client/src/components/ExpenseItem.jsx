import React from 'react';
import { ListGroupItem, Badge } from 'reactstrap';
import { Link } from 'react-router-dom';
import momment from 'moment';
import { ConfirmModel } from './ConfirmModel';

const ExpenseItem = ({ item, onDelete }) => {
  return (
    <ListGroupItem>
      <div className='float-left'>
        <span>{item.description ? item.description : 'Item'}</span>
        <Badge color='info' className='mx-3'>
          {item.amount} AED
        </Badge>
        <div className='text-muted'>Date: {momment(item.created).format('LL')}</div>
      </div>
      <div className='float-right '>
        {/* Edit Page */}
        <Link
          className='btn text-info mx-1'
          to={{
            pathname: '/edit',
            state: { item },
          }}>
          <i className='fas fa-edit' style={{ fontSize: '1.3rem' }}></i>
        </Link>
        {/* Delete Page */}
        <ConfirmModel onDelete={onDelete} item={item} />
      </div>
    </ListGroupItem>
  );
};
export { ExpenseItem };
