import React, { useState, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getToken, removeUserSession } from '../utils/index';
import { verifyToken } from '../api/auth';
import { NavBar } from '../components/NavBar';

// handle the private routes
function SecuredRoute({ component: Component, ...rest }) {
  const [auth, setAuth] = useState(false);
  const [isTokenValidated, setIsTokenValidated] = useState(false);

  useEffect(() => {
    const token = getToken();
    // if No token Skip to routes
    if (!token) {
      setIsTokenValidated(true);
      return;
    }
    // else verify token
    verifyToken(token)
      .then((response) => {
        setAuth(true);
      })
      .catch((err) => {
        setAuth(false);
        removeUserSession();
      })
      .then(() => {
        setIsTokenValidated(true);
      });
  }, []);
  // Flag to wait until validate
  if (!isTokenValidated) return <div></div>;
  // Route
  return (
    <React.Fragment>
      <NavBar />
      <Route
        {...rest}
        render={(props) =>
          auth ? <Component {...props} /> : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }
      />
    </React.Fragment>
  );
}

export { SecuredRoute };
