// return the token from the session storage
export const getToken = () => {
  return localStorage.getItem('SESSION_TOKEN') || null;
};

// remove the token and user from the session storage
export const removeUserSession = () => {
  localStorage.removeItem('SESSION_TOKEN');
};

// set the token and user from the session storage
export const setUserSession = (token) => {
  localStorage.setItem('SESSION_TOKEN', token);
};
