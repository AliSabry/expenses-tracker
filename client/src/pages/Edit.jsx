import React, { Component } from 'react';
import { AddFormBody } from '../components/AddFormBody';
import { Container, Col, Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import { apiEditExpense } from '../api/expense';

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: null,
      updated: false,
    };
  }
  componentDidUpdate() {
    const { updated } = this.state;
    if (updated)
      setTimeout(() => {
        this.props.history.push('/');
      }, 1500);
  }
  handelSubmit = (values, bag) => {
    const {
      item: { _id },
    } = this.props.location.state;
    values._id = _id;
    apiEditExpense(values)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          const {
            data: { msg },
          } = data;
          this.setState({ errors: <Alert color='success'>{msg}</Alert> });
          this.setState({ updated: true });
        }
      })
      .catch((err) => {
        console.log(err.response);
        const {
          response: {
            data: { message },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{message ? message : 'Server error'}</Alert> });
        bag.setSubmitting(false);
      });
  };
  render() {
    let item;
    try {
      item = this.props.location.state.item;
    } catch (error) {
      item = undefined;
    }
    if (!item) this.props.history.push('/');
    const { errors } = this.state;

    return (
      <Container className='themed-container mt-4'>
        <Link to='/' size='lg' className='btn text-muted m-1 p-0'>
          <i className='fas fa-chevron-left' style={{ fontSize: '1.4rem' }}></i>
        </Link>
        <hr />
        <Col xs={{ size: 10, offset: 1 }} md='6'>
          <h3> Edit Expenses</h3>
          <hr />
          {errors}
          <AddFormBody btnTxt='Update Expense' expense={item} onSubmitAction={this.handelSubmit} />
        </Col>
      </Container>
    );
  }
}

export { Edit };
