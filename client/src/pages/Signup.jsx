import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { apiSignup } from '../api/auth';
import { Row, Card, Col, Button, FormGroup, FormFeedback, Label, Input, Alert } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: null,
    };
  }

  // Form Submit handel
  handleFormSubmit = (values, bag) => {
    // email and password
    console.log(values);
    apiSignup(values)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          const {
            data: { msg },
          } = data;
          this.setState({ errors: <Alert color='success'>{msg}</Alert> });
        }
        // redirect to home page
        // this.props.history.push('/');
      })
      .catch((err) => {
        console.log(err.response.data);

        const {
          response: {
            data: { message },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{message ? message : 'Server error'}</Alert> });
        bag.setSubmitting(false);
      });
  };

  render() {
    const { errors } = this.state;
    const centerRow = { alignItems: 'center', height: '90vh' };
    return (
      <Row className='justify-content-center m-0' style={centerRow}>
        <Col xs='10' sm='8' md='5'>
          <Card body>
            <h3 className='text-center'>Create new account</h3>
            <hr />
            {/* Errors Alert */}
            {errors}
            {/* Formik Form Handel with Yup validation */}
            <Formik
              initialValues={{
                name: '',
                email: '',
                password: '',
                confirmPassword: '',
              }}
              onSubmit={this.handleFormSubmit}
              validationSchema={Yup.object().shape({
                name: Yup.string().required(),
                email: Yup.string().email().required(),
                password: Yup.string().required().min(8),
                confirmPassword: Yup.string()
                  .required()
                  .min(8)
                  .oneOf([Yup.ref('password')], 'Both password need to be the same'),
              })}>
              {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched }) => (
                <div>
                  <FormGroup>
                    <Label>Name</Label>
                    <Input
                      invalid={errors.name && touched.name}
                      valid={!errors.name && touched.name}
                      name='name'
                      type='text'
                      placeholder='enter your name'
                      onBlur={handleBlur}
                      onChange={handleChange}></Input>
                    {errors.name && touched.name ? <FormFeedback>{errors.name}</FormFeedback> : null}
                  </FormGroup>
                  <FormGroup>
                    <Label>Email</Label>
                    <Input
                      invalid={errors.email && touched.email}
                      valid={!errors.email && touched.email}
                      name='email'
                      type='email'
                      placeholder='example@g.com'
                      onBlur={handleBlur}
                      onChange={handleChange}></Input>
                    {errors.email && touched.email ? <FormFeedback>{errors.email}</FormFeedback> : null}
                  </FormGroup>
                  <FormGroup>
                    <Label>Password</Label>
                    <Input
                      invalid={errors.password && touched.password}
                      valid={!errors.password && touched.password}
                      name='password'
                      type='password'
                      placeholder='enter your password'
                      onBlur={handleBlur}
                      onChange={handleChange}></Input>
                    {errors.password && touched.password ? <FormFeedback>{errors.password}</FormFeedback> : null}
                  </FormGroup>
                  <FormGroup>
                    <Label>Confirm password</Label>
                    <Input
                      invalid={errors.confirmPassword && touched.confirmPassword}
                      valid={!errors.confirmPassword && touched.confirmPassword}
                      name='confirmPassword'
                      type='password'
                      placeholder='Confirm your password'
                      onBlur={handleBlur}
                      onChange={handleChange}></Input>
                    {errors.confirmPassword && touched.confirmPassword ? (
                      <FormFeedback>{errors.confirmPassword}</FormFeedback>
                    ) : null}
                  </FormGroup>
                  <div className='text-center py-2'>
                    <Button color='dark' className='w-50' onClick={handleSubmit} disabled={!isValid || isSubmitting}>
                      Create Account
                    </Button>
                  </div>
                </div>
              )}
            </Formik>
            <div className='text-center py-2'>
              <Label>Have an account ?</Label>
              <Link to='/login'> Sign in</Link>
            </div>
          </Card>
        </Col>
      </Row>
    );
  }
}
export { Signup };
