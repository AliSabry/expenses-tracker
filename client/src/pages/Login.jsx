import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { apiLogin } from '../api/auth';
import { setUserSession } from '../utils/index';
import { Row, Card, Col, Button, FormGroup, FormFeedback, Label, Input, Alert } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: null,
    };
  }

  // Form Submit handel
  handleFormSubmit = (values, bag) => {
    // email and password
    apiLogin(values)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          const {
            data: { token },
          } = data;
          setUserSession(token);
          this.setState({ errors: <Alert color='success'>login success</Alert> });
        }
        // redirect to home page
        this.props.history.push('/');
      })
      .catch((err) => {
        const {
          response: {
            data: { error },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{error ? error : 'Server error'}</Alert> });
        bag.setSubmitting(false);
      });
  };

  render() {
    const { errors } = this.state;
    const centerRow = { alignItems: 'center', height: '70vh' };
    return (
      <Row className='justify-content-center m-0' style={centerRow}>
        <Col xs='10' sm='8' md='5'>
          <Card body>
            <h3 className='text-center'>Sign in</h3>
            <hr />
            {/* Errors Alert */}
            {errors}
            {/* Formik Form Handel with Yup validation */}
            <Formik
              initialValues={{ email: 'a@a.com', password: '12345678' }}
              onSubmit={this.handleFormSubmit}
              validationSchema={Yup.object().shape({
                email: Yup.string().email().required(),
                password: Yup.string().required().min(8).required(),
              })}>
              {({ handleChange, handleSubmit, isValid, isSubmitting, handleBlur, errors, touched, values }) => (
                <div>
                  <FormGroup>
                    <Label>Email</Label>
                    <Input
                      invalid={errors.email && touched.email}
                      valid={!errors.email && touched.email}
                      name='email'
                      type='email'
                      value={values.email}
                      placeholder='example@g.com'
                      onBlur={handleBlur}
                      onChange={handleChange}></Input>
                    {errors.email && touched.email ? <FormFeedback>{errors.email}</FormFeedback> : null}
                  </FormGroup>
                  <FormGroup>
                    <Label>Password</Label>
                    <Input
                      invalid={errors.password && touched.password}
                      valid={!errors.password && touched.password}
                      name='password'
                      type='password'
                      value={values.password}
                      placeholder='enter your password'
                      onBlur={handleBlur}
                      onChange={handleChange}></Input>
                    {errors.password && touched.password ? <FormFeedback>{errors.password}</FormFeedback> : null}
                  </FormGroup>
                  <div className='text-center py-2'>
                    <Button color='dark' className='w-50' onClick={handleSubmit} disabled={!isValid || isSubmitting}>
                      Sign In
                    </Button>
                  </div>
                </div>
              )}
            </Formik>
            <div className='text-center py-2'>
              <Label>Don't have account ?</Label>
              <Link to='/signup'> Sign Up Now</Link>
            </div>
          </Card>
        </Col>
      </Row>
    );
  }
}
export { Login };
