import React, { Component } from 'react';
import { Alert, ListGroup, Container } from 'reactstrap';
import { ModelForm, Spinner, ExpenseItem, MonthSelector, Statistics } from '../components/components';
import { apiFetchExpense, apideleteExpense } from '../api/expense';
import moment from 'moment';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      expensesList: null,
      errors: null,
      selected: moment().month(),
      analysis: '',
    };
  }

  handelDelete = (e) => {
    console.log('delete');
    const expenseId = e.target.attributes.getNamedItem('data-id').value;
    apideleteExpense(expenseId)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          const {
            data: { msg },
          } = data;
          console.log(msg);
        }
      })
      .catch((err) => {
        console.log(err.response.data);
        const {
          response: {
            data: { message },
          },
        } = err;
        console.log(message);
      })
      .then(() => {
        this.handelRefresh(true);
      });
  };

  handelRefresh = (refresh) => {
    // refresh page after adding new item
    if (refresh) {
      const { selected } = this.state;
      this.handelFetchingData(selected);
    }
  };

  componentDidMount() {
    // Load all expense data
    const { selected } = this.state;
    this.handelFetchingData(selected);
  }

  handelSelectMonth = (selectedMonth) => {
    this.setState({ selected: selectedMonth });
    this.handelFetchingData(selectedMonth);
  };

  handelFetchingData = (selectedMonth) => {
    apiFetchExpense(selectedMonth)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          this.setState({ isLoading: false });

          const { expenses, Statistics } = data.data;

          if (expenses.length > 0) {
            this.setState({ expensesList: expenses });
            this.setState({ analysis: Statistics });
          } else {
            this.setState({ expensesList: null });
            this.setState({ analysis: '' });
          }
        }
      })
      .catch((err) => {
        console.log(err.response.data);
        const {
          response: {
            data: { message },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{message ? message : 'Server error'}</Alert> });
      });
  };

  render() {
    const { errors } = this.state;
    const { isLoading } = this.state;
    const { expensesList } = this.state;
    const { selected } = this.state;
    const { analysis } = this.state;
    return (
      <div>
        <ModelForm onRefresh={this.handelRefresh} />
        <Container className='themed-container mt-4' style={{ marginBottom: 105 }}>
          <h3>Expenses List</h3>
          <hr />
          <Statistics data={analysis} />
          <MonthSelector selected={selected} onChange={this.handelSelectMonth} />
          {errors}
          {isLoading ? <Spinner size={50} color='#111' /> : null}
          <ListGroup style={{ zIndex: 10 }}>
            {expensesList ? (
              expensesList.map((item) => <ExpenseItem key={item._id} item={item} onDelete={this.handelDelete} />)
            ) : (
              <h5 className='text-muted'>
                No Expenses <i className='far fa-smile-beam'></i>
              </h5>
            )}
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export { Home };
