import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Col, Container, Alert } from 'reactstrap';

import { ProfileForm, PasswordForm, ConfirmModel } from '../components/components';
import { apiEditUser, apiChangePassword, apideleteUser } from '../api/user';
import { removeUserSession } from '../utils/ManageSession';
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: null,
      type: '',
    };
  }
  handleEditProfile = (values, bag) => {
    this.setState({ type: 'profile' });
    apiEditUser(values)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          const {
            data: { msg },
          } = data;
          this.setState({ errors: <Alert color='success'>{msg}</Alert> });
          bag.setSubmitting(false);
        }
      })
      .catch((err) => {
        console.log(err.response);
        const {
          response: {
            data: { message },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{message ? message : 'Server error'}</Alert> });
        bag.setSubmitting(false);
      });
  };
  handleChangePassword = (values, bag) => {
    this.setState({ type: 'pass' });
    apiChangePassword(values)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          const {
            data: { msg },
          } = data;
          this.setState({ errors: <Alert color='success'>{msg}</Alert> });
          bag.setSubmitting(false);
        }
      })
      .catch((err) => {
        console.log(err.response);
        const {
          response: {
            data: {
              error: { message },
            },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{message ? message : 'Server error'}</Alert> });
        bag.setSubmitting(false);
      });
  };
  handleDelete = () => {
    this.setState({ type: 'del' });
    apideleteUser()
      .then((res) => {
        const { data } = res;
        if (data.success) {
          removeUserSession();
          this.props.history.push('/login');
        }
      })
      .catch((err) => {
        console.log(err.response);
        const {
          response: {
            data: {
              statusText,
            },
          },
        } = err;
        this.setState({ errors: <Alert color='danger'>{statusText ? statusText : 'Server error'}</Alert> });
      });
  };

  render() {
    const { errors, type } = this.state;
    const { name, email } = this.props.location.state;
    return (
      <Container className='themed-container my-4'>
        <Link to='/' size='lg' className='btn text-muted m-1 p-0'>
          <i className='fas fa-chevron-left' style={{ fontSize: '1.4rem' }}></i>
        </Link>
        <hr />
        <Col xs={{ size: 10, offset: 0 }} md='6'>
          <h4> Profile Settings</h4>
          {type === 'profile' ? errors : null}
          <ProfileForm user={{ name: name, email: email }} handleFormSubmit={this.handleEditProfile} />

          <h4 className='mt-5'> Change Password</h4>
          <hr />
          {type === 'pass' ? errors : null}
          <PasswordForm handleFormSubmit={this.handleChangePassword} />

          <h4 className='mt-5'>Delete Account</h4>
          <hr />
          {type === 'del' ? errors : null}
          <ConfirmModel className='p-0'
            onDelete={this.handleDelete}
            item={(name, { description: 'your account', setLabel: 'Delete Account' })}
          />
        </Col>
        <hr />
      </Container>
    );
  }
}

export { Settings };
