import axios from 'axios';
import setAuthHeaders from './setAuthHeaders';
import { API_CONST, getToken } from '../utils/index';

export const apiEditUser = (user) => {
  setAuthHeaders(getToken());
  return axios.put(`${API_CONST}/user/update`, user);
};
export const apiChangePassword = (data) => {
  setAuthHeaders(getToken());
  return axios.put(`${API_CONST}/user/password`, data);
};
export const apideleteUser = () => {
  setAuthHeaders(getToken());
  return axios.delete(`${API_CONST}/user/delete`, {});
};
