import axios from 'axios';
import setAuthHeaders from './setAuthHeaders';
import { API_CONST, getToken } from '../utils/index';

export const apiSaveExpense = (expense) => {
  setAuthHeaders(getToken());
  return axios.post(`${API_CONST}/expense/create`, expense);
};
export const apiFetchExpense = (month) => {
  setAuthHeaders(getToken());
  return axios.get(`${API_CONST}/expense/get`, { params: { month } });
};
export const apiEditExpense = (expense) => {
  setAuthHeaders(getToken());
  return axios.put(`${API_CONST}/expense/${expense._id}`, expense);
};
export const apideleteExpense = (_id) => {
  setAuthHeaders(getToken());
  return axios.delete(`${API_CONST}/expense/${_id}`, {});
};
