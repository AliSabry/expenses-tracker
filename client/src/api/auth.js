import axios from 'axios';
import setAuthHeaders from './setAuthHeaders';
import { API_CONST } from '../utils/index';

export const apiLogin = (user) => {
  return axios.post(`${API_CONST}/auth/login`, user);
};

export const apiSignup = (user) => {
  return axios.post(`${API_CONST}/auth/register`, user);
};

export const apiProfile = (token) => {
  setAuthHeaders(token);
  return axios.get(`${API_CONST}/user/profile`);
};

export const verifyToken = (token) => {
  setAuthHeaders(token);
  return axios.post(`${API_CONST}/auth/verify`);
};
