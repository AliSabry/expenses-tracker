import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { SecuredRoute } from './utils/index';
import { Home, Login, Signup, Edit, Settings } from './pages/pages';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          {/* Public Routes */}
          <Route path='/Login' component={Login} exact />
          <Route path='/Signup' component={Signup} exact />
          {/* Auth Routes */}
          <SecuredRoute path='/' component={Home} exact />
          <SecuredRoute path='/edit' component={Edit} exact />
          <SecuredRoute path='/Setting' component={Settings} exact />
        </Switch>
      </React.Fragment>
    );
  }
}
export { App };
